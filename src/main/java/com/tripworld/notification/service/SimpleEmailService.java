package com.tripworld.notification.service;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.Destination;
import com.amazonaws.services.simpleemail.model.SendTemplatedEmailRequest;
import com.amazonaws.services.simpleemail.model.SendTemplatedEmailResult;
import java.util.ArrayList;
import java.util.List;


public class SimpleEmailService {


	public void sendEmail(final String recipient, final String emailBody) {
        System.out.println("sending email - "+ recipient + " - " + emailBody);
        String region = System.getenv("SES_AWS_REGION");
        String senderEmailId = System.getenv("SOURCE_EMAIL_ID");
        String templateName = System.getenv("TEMPLATE_NAME");
        AmazonSimpleEmailService amazonSimpleEmailService = AmazonSimpleEmailServiceClientBuilder.standard().withRegion(Regions.fromName(region)).build();
        try {
            Destination destination = new Destination();
            List<String> toAddresses = new ArrayList<String>();

            toAddresses.add(recipient);
            destination.setToAddresses(toAddresses);
            SendTemplatedEmailRequest templatedEmailRequest = new SendTemplatedEmailRequest();
            templatedEmailRequest.withDestination(destination);
            templatedEmailRequest.withTemplate(templateName);
            templatedEmailRequest.withTemplateData(emailBody);
            templatedEmailRequest.withSource(senderEmailId);
            SendTemplatedEmailResult templatedEmailResult = amazonSimpleEmailService.sendTemplatedEmail(templatedEmailRequest);
            System.out.println(templatedEmailResult.getMessageId());
            if(templatedEmailResult.getSdkHttpMetadata()!=null) {
                System.out.println("getHttpStatusCode():"+templatedEmailResult.getSdkHttpMetadata().getHttpStatusCode());
            }
            
            System.out.println("sendig email..." + senderEmailId);
        } catch (Exception exception) {
            System.err.println("error while sending email!! " + exception.getMessage());
            exception.printStackTrace();
        }
    }

}
