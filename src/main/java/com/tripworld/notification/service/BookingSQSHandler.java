package com.tripworld.notification.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.springframework.web.client.RestTemplate;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tripworld.domain.booking.Order;

public class BookingSQSHandler implements RequestHandler<Object, String> {

    @SuppressWarnings("unchecked")
    public String handleRequest(final Object object, final Context context) {
        String recipient = System.getenv("DEFAULT_RECEIVER");
        String orderURL = System.getenv("API_SERVER") + "/orders/";
        context.getLogger().log("----------------------------Input: " + object.getClass() + " ----" + object);
        Map<String, List<Map<String, String>>> sqsMap = (Map<String, List<Map<String, String>>>) object;
        List<Map<String, String>> sqsMessageBody = (List<Map<String, String>>) sqsMap.get("Records");
        Map<String, String> body = (Map<String, String>) sqsMessageBody.get(0);
        String payload = body.get("body");
        
        RestTemplate restTemplate = new RestTemplate();
        Order order = restTemplate.getForObject(orderURL + payload, Order.class);
        context.getLogger().log("----------------------------api: " + order.getOrderNumber());
        // prepare object for template data.
        
        //customerName, orderNumber
        Map<String, String> emailBodyData = new HashMap<String, String>();
        emailBodyData.put("customerName", order.getCustomer().getFirstName() + " " + order.getCustomer().getLastName());
        emailBodyData.put("orderNumber", order.getOrderNumber());
        emailBodyData.put("orderNumber", order.getOrderNumber());
        emailBodyData.put("productAddress", order.getPartner().getPartnerAddress());
        emailBodyData.put("orderPin", order.getOrderPin());
        emailBodyData.put("partnerPartnerName", order.getPartner().getPartnerName());
        emailBodyData.put("partnerPartnerAddress", order.getPartner().getPartnerHqAddress());
        emailBodyData.put("tripworldSupportPhone", System.getenv("TW_PHONE"));
        emailBodyData.put("tripworldSupportEmail", System.getenv("TW_SUPPORT_EMAIL"));
        emailBodyData.put("searchRequestNightStays", String.valueOf(order.getSearchRequest().getNightStays()));
        emailBodyData.put("orderItemsLength", String.valueOf(order.getOrderItems().size()));
        emailBodyData.put("searchRequestCheckIn", new LocalDate(order.getSearchRequest().getCheckIn()).toString("EEE, dd MMMM yyyy"));
        emailBodyData.put("partnerCheckInTime", order.getPartner().getCheckInTime());
        emailBodyData.put("searchRequestCheckOut", new LocalDate(order.getSearchRequest().getCheckOut()).toString("EEE, dd MMMM yyyy"));
        emailBodyData.put("partnerCheckOutTime", order.getPartner().getCheckOutTime());
        emailBodyData.put("tripworldAddress", System.getenv("TW_ADDRESS"));
        emailBodyData.put("tripworldEmail1", System.getenv("TW_EMAIL1"));
        emailBodyData.put("tripworldSupportPhone1", System.getenv("TW_PHONE1"));
        emailBodyData.put("tripworldCopyYear", System.getenv("COPY_YEAR"));
        emailBodyData.put("orderPin", System.getenv("COPY_YEAR"));
        
        if(recipient == null) {
            recipient = order.getCustomer().getEmail();
        }
        String emailTemplateData = null;
        try {
            emailTemplateData = new ObjectMapper().writeValueAsString(emailBodyData);
        } catch (JsonProcessingException e) {
            context.getLogger().log("Error preparing email template. order :  "+ order.getOrderNumber());
        }
        context.getLogger().log("---------------------------- emailTemplateData: " + emailTemplateData);

        if(emailTemplateData != null) {
        SimpleEmailService simpleEmailService = new SimpleEmailService();
        simpleEmailService.sendEmail(recipient, emailTemplateData);
        } else {
            context.getLogger().log("email template data not ready. skipping email sending. for order: " + order.getOrderNumber());
        }
        return "Booking notification executed for - " + payload;
    }
    
}
