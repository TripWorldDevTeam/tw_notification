package com.tripworld.notification.service;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MailPropertyHandler implements RequestHandler<Object, String> {

	public String handleRequest(final Object object, final Context context) {
		String recipient = System.getenv("DEFAULT_RECEIVER");
		
		Map<String, List<Map<String, String>>> sqsMap = (Map<String, List<Map<String, String>>>) object;
        List<Map<String, String>> sqsMessageBody = (List<Map<String, String>>) sqsMap.get("Records");
        Map<String, String> body = (Map<String, String>) sqsMessageBody.get(0);
        String payload = body.get("body");
        
        Map<String, String> emailBodyData = new HashMap<String, String>();
        emailBodyData.put("email", payload);
        
        if(payload != null) {
            recipient = payload;
        }

        String emailTemplateData = null;
        try {
            emailTemplateData = new ObjectMapper().writeValueAsString(emailBodyData);
        } catch (JsonProcessingException e) {
            context.getLogger().log("Error preparing email template. for Partner :  "+ payload);
        }
        context.getLogger().log("---------------------------- emailTemplateData: " + emailTemplateData);

        if(emailTemplateData != null) {
        SimpleEmailService simpleEmailService = new SimpleEmailService();
        simpleEmailService.sendEmail(recipient, emailTemplateData);
        } else {
            context.getLogger().log("email template data not ready. skipping email sending. for Partner: " + payload);
        }
        return "Notification sent for - " + payload;
		
	}

	

}
